
static char help[] = "Solves a tridiagonal linear system.\n\n";

/*T
   Concepts: KSP^basic parallel example;
   Processors: n
T*/

/*
  Include "petscksp.h" so that we can use KSP solvers.  Note that this file
  automatically includes:
     petscsys.h       - base PETSc routines   petscvec.h - vectors
     petscmat.h - matrices
     petscis.h     - index sets            petscksp.h - Krylov subspace methods
     petscviewer.h - viewers               petscpc.h  - preconditioners

  Note:  The corresponding uniprocessor example is ex1.c
*/
#include <petscksp.h>

extern double eps0_GLB;		       // permitivity of free space (vacuum) [F/m]

extern double* rho_GLB;                // charge density [C/m]
extern double* phi_GLB;                // potential [V]
extern double* Efield_GLB;             // electric field [V/m]

extern int ngrid_GLB;                  // no. of grid points
extern double xl_GLB, xr_GLB;          // locations of left and right boundaries [m]
extern double phil_GLB, phir_GLB;      // Dirichlet BCs: prescribed potentials at left and right boundaries [V]
extern double dx_GLB, L_GLB;           // grid and domain sizes [m]

#undef __FUNCT__
#define __FUNCT__ "solve_fields"
PetscErrorCode solve_fields(void /* int argc,char **args */)
{
  Vec            phi, b;          /* approx solution, RHS */
  Mat            A;                /* linear system matrix */
  KSP            ksp;              /* linear solver context */
  PC             pc;               /* preconditioner context */
  PetscErrorCode ierr;
  PetscInt       i,n,col[3],rstart,rend,nlocal;
#if 0
  PetscReal      norm,tol=1000.*PETSC_MACHINE_EPSILON;  /* norm of solution error */
  PetscInt	 its
  PetscScalar    neg_one = -1.0,one = 1.0;
#endif
  PetscScalar	 value[3];
#if 0
  /* Visualization contexts */
  PetscViewer	 Aviewer, phiviewer, bviewer; 
  PetscViewer 	 xviewer, EFviewer, Cviewer;
#endif
  Vec		 x;			/* Positions of grid points [m] */
  Vec		 EField;		/* Electric field [V/m] */
  PetscInt	 Ngrid;			/* No. of grid points */
  PetscScalar	 xl, xr, phil, phir;	/* Boundary locations [m] and phi values [V] */
  PetscScalar	 L, dx, dxsq;		/* Domain, grid sizes [m] and square of grid size [m^2] */
  PetscScalar	 oned2dx;
  Mat		 C;

  PetscScalar*	 phiarray;		/* Array to contain calculated phi */
  PetscScalar*	 EFieldarray;		/* Array to contain calculated E-field */

  /*
     Set and calculate simulation parameters
  */
  Ngrid = ngrid_GLB;

  /* Set boundary conditions (Dirichlet) */
  xl = xl_GLB; phil = phil_GLB;
  xr = xr_GLB; phir = phir_GLB;

  L = L_GLB;
  dx = dx_GLB;
  dxsq = dx*dx;
  oned2dx = 0.5/dx;
  n = Ngrid;

  /* 
     Initialize PETSc calculations
  */
//  PetscInitialize(&argc,&args,(char*)0,help);
//  ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);

  /*
     Open files for outputting matrix A, solution phi and RHS b.
     The files are in ASCII MATLAB format.
  */
#if 0
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "Amatrix.m", &Aviewer); CHKERRQ(ierr);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "phisolution.m", &phiviewer); CHKERRQ(ierr);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "bRHS.m", &bviewer); CHKERRQ(ierr);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "xgridpos.m", &xviewer); CHKERRQ(ierr);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "EField.m", &EFviewer); CHKERRQ(ierr);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "Cmatrix.m", &Cviewer); CHKERRQ(ierr);

  ierr = PetscViewerPushFormat(Aviewer, PETSC_VIEWER_ASCII_MATLAB);
  ierr = PetscViewerPushFormat(phiviewer, PETSC_VIEWER_ASCII_MATLAB);
  ierr = PetscViewerPushFormat(bviewer, PETSC_VIEWER_ASCII_MATLAB);
  ierr = PetscViewerPushFormat(xviewer, PETSC_VIEWER_ASCII_MATLAB);
  ierr = PetscViewerPushFormat(EFviewer, PETSC_VIEWER_ASCII_MATLAB);
  ierr = PetscViewerPushFormat(Cviewer, PETSC_VIEWER_ASCII_MATLAB);
#endif

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Create vectors.  Note that we form 1 vector from scratch and
     then duplicate as needed. For this simple case let PETSc decide how
     many elements of the vector are stored on each processor. The second
     argument to VecSetSizes() below causes PETSc to decide.
  */
  ierr = VecCreate(PETSC_COMM_WORLD,&phi);CHKERRQ(ierr);
  ierr = VecSetSizes(phi,PETSC_DECIDE,n);CHKERRQ(ierr);
  ierr = VecSetFromOptions(phi);CHKERRQ(ierr);
  ierr = VecDuplicate(phi,&b);CHKERRQ(ierr);
  ierr = VecDuplicate(phi,&x);CHKERRQ(ierr);
  ierr = VecDuplicate(phi,&EField);CHKERRQ(ierr);

  /* Identify the starting and ending mesh points on each
     processor for the interior part of the mesh. We let PETSc decide
     above. */

  ierr = VecGetOwnershipRange(phi,&rstart,&rend);CHKERRQ(ierr);
  ierr = VecGetLocalSize(phi,&nlocal);CHKERRQ(ierr);

  /*
     Create matrix.  When using MatCreate(), the matrix format can
     be specified at runtime.

     Performance tuning note:  For problems of substantial size,
     preallocation of matrix memory is crucial for attaining good
     performance. See the matrix chapter of the users manual for details.

     We pass in nlocal as the "local" size of the matrix to force it
     to have the same parallel layout as the vector created above.
  */
  ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,nlocal,nlocal,n,n);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);

  ierr = MatCreate(PETSC_COMM_WORLD,&C);CHKERRQ(ierr);
  ierr = MatSetSizes(C,nlocal,nlocal,n,n);CHKERRQ(ierr);
  ierr = MatSetFromOptions(C);CHKERRQ(ierr);
  ierr = MatSetUp(C);CHKERRQ(ierr);

  /*
     Assemble matrices, RHS and grid positions.

     The linear system is distributed across the processors by
     chunks of contiguous rows, which correspond to contiguous
     sections of the mesh on which the problem is discretized.
     For matrix assembly, each processor contributes entries for
     the part that it owns locally.
  */

  if (!rstart) {
    rstart = 1;
    i      = 0; 

    /* Matrix A */
    value[0] = 1.0;
    ierr   = MatSetValues(A,1,&i,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* RHS */
    value[0] = phil;
    ierr   = VecSetValues(b,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* Grid positions */
    value[0] = xl + i*dx;
    ierr   = VecSetValues(x,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* Matrix C */
    col[0] = i; col[1] = i+1; col[2] = i+2;
    value[0] = 3.0; value[1] = -4.0; value[2] = 1.0;
    ierr = MatSetValues(C,1,&i,3,col,value,INSERT_VALUES);CHKERRQ(ierr);
  }
  if (rend == n) {
    rend = n-1;
    i    = n-1; 

    /* Matrix A */
    value[0] = 1.0;
    ierr = MatSetValues(A,1,&i,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* RHS */
    value[0] = phir;
    ierr   = VecSetValues(b,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* Grid positions */
    value[0] = xl + i*dx;
    ierr   = VecSetValues(x,1,&i,value,INSERT_VALUES);CHKERRQ(ierr);

    /* Matrix C */
    col[0] = i-2; col[1] = i-1; col[2] = i;
    value[0] = -1.0; value[1] = 4.0; value[2] = -3.0;
    ierr = MatSetValues(C,1,&i,3,col,value,INSERT_VALUES);CHKERRQ(ierr);
  }

  /* Set entries corresponding to the mesh interior */

  /* Matrix A */
  value[0] = -1.0; value[1] = 2.0; value[2] = -1.0;
  for (i=rstart; i<rend; i++) {
    col[0] = i-1; col[1] = i; col[2] = i+1;
    ierr   = MatSetValues(A,1,&i,3,col,value,INSERT_VALUES);CHKERRQ(ierr);
  }

  for (i=rstart; i<rend; i++) {
    /* Grid positions */
    value[0] = xl + i*dx;
    ierr = VecSetValues(x,1,&i,value,INSERT_VALUES);

    /* RHS */
//    value[0] = 0.0*(PETSC_PI/L)*(PETSC_PI/L)*PetscSinScalar(2.0*PETSC_PI*i*dx/L)*dxsq;
    value[0] = rho_GLB[i]*dxsq/eps0_GLB;
    ierr = VecSetValues(b,1,&i,value,INSERT_VALUES);
  }

  /* Matrix C */
  value[0] = 1.0; value[1] = 0.0; value[2] = -1.0;
  for (i=rstart; i<rend; i++) {
    col[0] = i-1; col[1] = i; col[2] = i+1;
    ierr   = MatSetValues(C,1,&i,3,col,value,INSERT_VALUES);CHKERRQ(ierr);
  }

  /* Assemble matrices, RHS and grid positions */
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(b);
  ierr = VecAssemblyEnd(b);
  ierr = VecAssemblyBegin(x);
  ierr = VecAssemblyEnd(x);
  ierr = MatAssemblyBegin(C,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(C,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                Create the linear solver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Create linear solver context
  */
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

  /*
     Set operators. Here the matrix that defines the linear system
     also serves as the preconditioning matrix.
  */
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);

  /*
     Set linear solver defaults for this problem (optional).
     - By extracting the KSP and PC contexts from the KSP context,
       we can then directly call any KSP and PC routines to set
       various options.
     - The following four statements are optional; all of these
       parameters could alternatively be specified at runtime via
       KSPSetFromOptions();
  */
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCJACOBI);CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,1.e-7,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);

  /*
    Set runtime options, e.g.,
        -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
    These options will override those specified above as long as
    KSPSetFromOptions() is called _after_ any other customization
    routines.
  */
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Solve the linear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Solve linear system
  */
  ierr = KSPSolve(ksp,b,phi);CHKERRQ(ierr);

  /*
     View solver info; we could instead use the option -ksp_view to
     print this info to the screen at the conclusion of KSPSolve().
  */
//  ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			Calculate electric field
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = MatMult(C,phi,EField); CHKERRQ(ierr);
  ierr = VecScale(EField,oned2dx); CHKERRQ(ierr);

  /*
     Output results (e.g. matrices) to files.
   */
#if 0
  ierr = MatView(A, Aviewer); CHKERRQ(ierr);
  ierr = VecView(phi, phiviewer); CHKERRQ(ierr);
  ierr = VecView(b, bviewer); CHKERRQ(ierr);
  ierr = VecView(x, xviewer); CHKERRQ(ierr);
  ierr = VecView(EField, EFviewer); CHKERRQ(ierr);
  ierr = MatView(C, Cviewer); CHKERRQ(ierr);
#endif
  /*
     Copy phi and E-field results
   */
  if (rstart==1) rstart=0;
  if (rend==n-1) rend=n;
  ierr = VecGetArray(phi, &phiarray); CHKERRQ(ierr);
  ierr = VecGetArray(EField, &EFieldarray); CHKERRQ(ierr);
  for (i=rstart; i<rend; i++) {
    phi_GLB[i] = phiarray[i];
    Efield_GLB[i] = EFieldarray[i];
  }
  ierr = VecRestoreArray(phi, &phiarray); CHKERRQ(ierr);
  ierr = VecRestoreArray(EField, &EFieldarray); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Check solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
#if 0
  /*
     Check the error
  */
  ierr = VecAXPY(phi,neg_one,u);CHKERRQ(ierr);
  ierr = VecNorm(phi,NORM_2,&norm);CHKERRQ(ierr);
  ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);
  if (norm > tol) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of error %g, Iterations %D\n",(double)norm,its);CHKERRQ(ierr);
  }
#endif
  /*
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
  */
  ierr = VecDestroy(&phi);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr); 
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&EField);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = MatDestroy(&C);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
#if 0
  ierr = PetscViewerPopFormat(Aviewer); CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(phiviewer); CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(bviewer); CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(xviewer); CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(EFviewer); CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(Cviewer); CHKERRQ(ierr);

  ierr = PetscViewerDestroy(&Aviewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&phiviewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&bviewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&xviewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&EFviewer); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&Cviewer); CHKERRQ(ierr);
#endif
  /*
     Always call PetscFinalize() before exiting a program.  This routine
       - finalizes the PETSc libraries as well as MPI
       - provides summary and diagnostic information if certain runtime
         options are chosen (e.g., -log_summary).
  */
//  ierr = PetscFinalize();
  PetscFunctionReturn(0);
}
