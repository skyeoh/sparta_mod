/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "mpi.h"
#include "sparta.h"
#include "input.h"

#include <petscksp.h> // Yeoh addition

using namespace SPARTA_NS;

/* ----- start Yeoh addition ----- */

/* Constant */
double eps0_GLB = 8.85418782E-12;	// permitivity of free space (vacuum) [F/m]

/* Global variables */
double* rho_GLB;		// charge density [C/m]
double* phi_GLB; 		// potential [V]
double* Efield_GLB;		// electric field [V/m]

int ngrid_GLB;			// no. of grid points
double xl_GLB, xr_GLB;		// locations of left and right boundaries [m]
double phil_GLB, phir_GLB;	// Dirichlet BCs: prescribed potentials at left and right boundaries [V]
double dx_GLB, L_GLB;		// grid and domain sizes [m]

double alpha_GLB = 0.5;			// relaxation parameter
double rho_relax_init_GLB = 0.0;	// initial value of rho_relax_GLB at all grid points [C/m]
double* rho_relax_GLB;			// adjusted charge density from relaxation [C/m]

int ntimestep_timeavg_GLB[2] = {1000, 6000};	// lower and upper limits of timestep range over which time-averaging is taken to reduce noise in interpolated charge density
int nsamp_timeavg_GLB;				// no. of samples taken in time-averaging process
double* rho_timeavg_GLB;			// time-averaged charge density [C/m]

double* rho_instant_GLB;			// instantaneous charge density [C/m]

int ntimestep_samp_start_gridprops_GLB = 100;		// no. of timestep at which to start sampling
int ntimestep_samp_every_gridprops_GLB = 100;		// no. of timesteps between samples
int ntimestep_samp_end_gridprops_GLB = 1000;		// no. of timestep at which to stop sampling

int ntimestep_samp_start_particleprops_GLB = 100;	// no. of timestep at which to start sampling
int ntimestep_samp_every_particleprops_GLB = 100;	// no. of timesteps between samples
int ntimestep_samp_end_particleprops_GLB = 1000;	// no. of timestep at which to stop sampling

int ntimestep_samp_start_vdf_GLB = 100;		// no. of timestep at which to start sampling
int ntimestep_samp_every_vdf_GLB = 100;		// no. of timesteps between samples
int ntimestep_samp_end_vdf_GLB = 1000;		// no. of timestep at which to stop sampling

int ntimestep_samp_start_vdf_inst_GLB = 499950;		// no. of timestep at which to start sampling
int ntimestep_samp_every_vdf_inst_GLB = 10;		// no. of timesteps between samples
int ntimestep_samp_end_vdf_inst_GLB = 510000;		// no. of timestep at which to stop sampling

int ntimestep_samp_start_partgenprops_fixemit_GLB = 499950;	// no. of timestep at which to start sampling
int ntimestep_samp_every_partgenprops_fixemit_GLB = 10;		// no. of timesteps between samples
int ntimestep_samp_end_partgenprops_fixemit_GLB = 510000;	// no. of timestep at which to stop sampling

int ntimestep_samp_start_partgenprops2_GLB = 499950;     // no. of timestep at which to start sampling
int ntimestep_samp_every_partgenprops2_GLB = 10;         // no. of timesteps between samples
int ntimestep_samp_end_partgenprops2_GLB = 510000;       // no. of timestep at which to stop sampling

FILE* fpartgenprops;

int Nsp_GLB = 2;		// no. of species

double* Nin_GLB;		// no. of real particles of a particular species to insert into domain
double* Nout_xl_GLB;		// no. of real particles of a particular species exiting domain through left boundary
double* Nout_xr_GLB;		// no. of real particles of a particular species exiting domain through right boundary

/* ------ end Yeoh addition ------ */

/* ----------------------------------------------------------------------
   main program to drive SPARTA
------------------------------------------------------------------------- */

int main(int argc, char **argv)
{
  PetscErrorCode ierr; // Yeoh addition

  MPI_Init(&argc,&argv);

  ierr = PetscInitialize(&argc,&argv,"sparta_petsc_options.txt",NULL); CHKERRQ(ierr); // Yeoh addition

  /* ----- start Yeoh addition ----- */

  Nin_GLB = new double [Nsp_GLB];
  Nout_xl_GLB = new double [Nsp_GLB];
  Nout_xr_GLB = new double [Nsp_GLB];

  /* ------ end Yeoh addition ------ */

  SPARTA *sparta = new SPARTA(argc,argv,MPI_COMM_WORLD);
  sparta->input->file();
  delete sparta;

  /* ----- start Yeoh addition ----- */

  /* Free memory allocated in 
     CreateGrid::command() in create_grid.cpp */
  delete [] rho_GLB;
  delete [] phi_GLB;
  delete [] Efield_GLB;
  delete [] rho_relax_GLB;
  delete [] rho_timeavg_GLB;
  delete [] rho_instant_GLB;

  delete [] Nin_GLB;
  delete [] Nout_xl_GLB;
  delete [] Nout_xr_GLB;

  /* ------ end Yeoh addition ------ */

  ierr = PetscFinalize(); CHKERRQ(ierr); // Yeoh addition

  MPI_Finalize();
}
